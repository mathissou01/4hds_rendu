<?php
require_once 'connection.php';

function Recuperation_data(String $sql = null)
{
    $check_query_message = null;
    $check_query = false;
    global $dbhost, $dbuser, $dbpass, $dbport, $db, $dbport;
    $check_query = false;
    $mysqli = new mysqli($dbhost,$dbuser,$dbpass,$db,$dbport);
    if($mysqli->connect_errno) {
        die('Could not connect: ' . $mysqli->connect_error);
    }
    $result = $mysqli->query($sql);
    $content = [];
    if ($result) {
        $check_query = true;
        while($row = $result->fetch_assoc()) {
            $content[] = $row;
        }
    }
    else{
        $check_query_message = $mysqli->error;
    }
    $mysqli->close();
    return ["content" => $content, "check_query" => $check_query, "check_query_message" => $check_query_message];
}

function UPDATE_DATA(String $sql_update = null){

    global $dbhost, $dbuser, $dbpass, $dbport, $db, $dbport;
    $mysqli = new mysqli($dbhost,$dbuser,$dbpass,$db,$dbport);
    if($mysqli->connect_errno) {
        die('Could not connect: ' . $mysqli->connect_error);
    }
    $retval = $mysqli->query($sql_update);
    try {
        $last_id = $mysqli->insert_id;
    } catch (\Throwable $th) {}
    if( $retval ) {
        $mysqli->close();
        return [false, $last_id, $mysqli->error];
    }
    $last_id = $mysqli->insert_id;
    $mysqli->close();
    return [true, $last_id];

}