<?php 

extract($_GET);

require_once 'connection.php';

if(isset($_GET)  && !empty($_GET)){

    global $dbhost, $dbuser, $dbpass, $dbport, $db, $dbport;
    $mysqli = new mysqli($dbhost,$dbuser,$dbpass,$db,$dbport);

    $control_form_err = true;

    $error_type = 0; // 0 pas d'erreur; 1 erreur type id; 2 erreur type fiche; 3 erreur de requete;

    $id_object = filter_input(INPUT_GET,'id_object',FILTER_SANITIZE_NUMBER_INT);
    $type_fiche = filter_input(INPUT_GET,'type_fiche',FILTER_SANITIZE_STRING);

    if (!ctype_digit($_GET['id_object'])){
        $error_type = 1;
    }
    if (!in_array($type_fiche, ['hot','cli','cham', 'res'])){
        $error_type = 2;
    }
    elseif (!empty($id_object)) {
        if ($type_fiche == "hot") {
            $base = "users";
        } 
        elseif ($type_fiche == "cli") {
            $base = "medicament";
        } 
        elseif ($type_fiche == "cham") {
            $base = "users";
        } 
        elseif ($type_fiche == "res") {
            $base = "users";
        }
        

        $sql = "DELETE FROM $base WHERE id = $id_object";
        $retval = $mysqli->query($sql);
   
        if(!$retval) {
            $error_type = 3;
            $error_message = $mysqli->error;
        }
        else {
            $control_form_err = false;
        }
    }  
  $mysqli->close();
}

if ($control_form_err || $error_type > 0) {
    if (strpos($src, "?") === false) {
        header("Location: ../" . $src . "?type_error=" . $error_type . "&id_error=" . $id_object . "&error_message=" . $error_message );
    } else {
        header("Location: ../" . $src . "&type_error=" . $error_type . "&id_error=" . $id_object);
    }
} 
else {
    header("Location: ../" . $src);
}
?>